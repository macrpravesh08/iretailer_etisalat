package partnerAPI.partner.model;

import java.util.Date;


public class User {

	private long USER_ID;
	private long RETAILER_HOUSE_ID;
	private String RETAILER_ID;
	private String REGION_CODE;
	private long AGENCY_ID;
	private String username;
	private String PASSWORD;
	private String USER_TYPE;
	private String FIRST_NAME;
	private String MIDDLE_NAME;
	private String LAST_NAME;
	private String DESIGNATION;
	private String DEPARTMENT;
	private String SECTION;
	private String CONTACT_TEL_NO;
	private String CONTACT_GSM_NO;
	private String EMAIL;
	private String INTERNAL_EMAIL;
	private String EMPLOYEE_NO;
	private String STATUS;
	private String LAST_OP_USER_ID;
	private Date LAST_OP_DATE_STAMP;
	private long GROUP_ID;
	private String PWD_ENCRYPTED_YN;
	private String DOCUMENT_TYPE;
	private String DOCUMENT_ID;
	private String VISA_NUMBER;
	private String RESIDENCY_FILE_NUMBER;
	private Date DOCUMENT_ISSUE_DATE;
	private Date DOCUMENT_EXPIRY_DATE;
	private String CONTACT_FAX;
	private long KR_STAFF_CODE;
	private String CBCM_USERNAME;
	private String ADDLN_MAPPING_ID;
	private String EIDA_FINGERPRINT_FLAG;
	private String OCR_FLAG;
	private String CREATED_OP_USER_ID;
	private Date CREATED_OP_DATE_STAMP;
	private String USP_ROLE_ID;
	private String BO_VERIFY_FLAG;
	private String BYPASS_EID_FLAG;
	private String BYPASS_PASSPORT_FLAG;
	private String BYPASS_GCCID_FLAG;
	private String GEO_FENCING_GPS_FLAG;
	public long getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(long uSER_ID) {
		USER_ID = uSER_ID;
	}
	public long getRETAILER_HOUSE_ID() {
		return RETAILER_HOUSE_ID;
	}
	public void setRETAILER_HOUSE_ID(long rETAILER_HOUSE_ID) {
		RETAILER_HOUSE_ID = rETAILER_HOUSE_ID;
	}
	public String getRETAILER_ID() {
		return RETAILER_ID;
	}
	public void setRETAILER_ID(String rETAILER_ID) {
		RETAILER_ID = rETAILER_ID;
	}
	public String getREGION_CODE() {
		return REGION_CODE;
	}
	public void setREGION_CODE(String rEGION_CODE) {
		REGION_CODE = rEGION_CODE;
	}
	public long getAGENCY_ID() {
		return AGENCY_ID;
	}
	public void setAGENCY_ID(long aGENCY_ID) {
		AGENCY_ID = aGENCY_ID;
	}
	public String getUSERNAME() {
		return username;
	}
	public void setUSERNAME(String username) {
		username = username;
	}
	public String getPASSWORD() {
		return PASSWORD;
	}
	public void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}
	public String getUSER_TYPE() {
		return USER_TYPE;
	}
	public void setUSER_TYPE(String uSER_TYPE) {
		USER_TYPE = uSER_TYPE;
	}
	public String getFIRST_NAME() {
		return FIRST_NAME;
	}
	public void setFIRST_NAME(String fIRST_NAME) {
		FIRST_NAME = fIRST_NAME;
	}
	public String getMIDDLE_NAME() {
		return MIDDLE_NAME;
	}
	public void setMIDDLE_NAME(String mIDDLE_NAME) {
		MIDDLE_NAME = mIDDLE_NAME;
	}
	public String getLAST_NAME() {
		return LAST_NAME;
	}
	public void setLAST_NAME(String lAST_NAME) {
		LAST_NAME = lAST_NAME;
	}
	public String getDESIGNATION() {
		return DESIGNATION;
	}
	public void setDESIGNATION(String dESIGNATION) {
		DESIGNATION = dESIGNATION;
	}
	public String getDEPARTMENT() {
		return DEPARTMENT;
	}
	public void setDEPARTMENT(String dEPARTMENT) {
		DEPARTMENT = dEPARTMENT;
	}
	public String getSECTION() {
		return SECTION;
	}
	public void setSECTION(String sECTION) {
		SECTION = sECTION;
	}
	public String getCONTACT_TEL_NO() {
		return CONTACT_TEL_NO;
	}
	public void setCONTACT_TEL_NO(String cONTACT_TEL_NO) {
		CONTACT_TEL_NO = cONTACT_TEL_NO;
	}
	public String getCONTACT_GSM_NO() {
		return CONTACT_GSM_NO;
	}
	public void setCONTACT_GSM_NO(String cONTACT_GSM_NO) {
		CONTACT_GSM_NO = cONTACT_GSM_NO;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	public String getINTERNAL_EMAIL() {
		return INTERNAL_EMAIL;
	}
	public void setINTERNAL_EMAIL(String iNTERNAL_EMAIL) {
		INTERNAL_EMAIL = iNTERNAL_EMAIL;
	}
	public String getEMPLOYEE_NO() {
		return EMPLOYEE_NO;
	}
	public void setEMPLOYEE_NO(String eMPLOYEE_NO) {
		EMPLOYEE_NO = eMPLOYEE_NO;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getLAST_OP_USER_ID() {
		return LAST_OP_USER_ID;
	}
	public void setLAST_OP_USER_ID(String lAST_OP_USER_ID) {
		LAST_OP_USER_ID = lAST_OP_USER_ID;
	}
	public Date getLAST_OP_DATE_STAMP() {
		return LAST_OP_DATE_STAMP;
	}
	public void setLAST_OP_DATE_STAMP(Date lAST_OP_DATE_STAMP) {
		LAST_OP_DATE_STAMP = lAST_OP_DATE_STAMP;
	}
	public long getGROUP_ID() {
		return GROUP_ID;
	}
	public void setGROUP_ID(long gROUP_ID) {
		GROUP_ID = gROUP_ID;
	}
	public String getPWD_ENCRYPTED_YN() {
		return PWD_ENCRYPTED_YN;
	}
	public void setPWD_ENCRYPTED_YN(String pWD_ENCRYPTED_YN) {
		PWD_ENCRYPTED_YN = pWD_ENCRYPTED_YN;
	}
	public String getDOCUMENT_TYPE() {
		return DOCUMENT_TYPE;
	}
	public void setDOCUMENT_TYPE(String dOCUMENT_TYPE) {
		DOCUMENT_TYPE = dOCUMENT_TYPE;
	}
	public String getDOCUMENT_ID() {
		return DOCUMENT_ID;
	}
	public void setDOCUMENT_ID(String dOCUMENT_ID) {
		DOCUMENT_ID = dOCUMENT_ID;
	}
	public String getVISA_NUMBER() {
		return VISA_NUMBER;
	}
	public void setVISA_NUMBER(String vISA_NUMBER) {
		VISA_NUMBER = vISA_NUMBER;
	}
	public String getRESIDENCY_FILE_NUMBER() {
		return RESIDENCY_FILE_NUMBER;
	}
	public void setRESIDENCY_FILE_NUMBER(String rESIDENCY_FILE_NUMBER) {
		RESIDENCY_FILE_NUMBER = rESIDENCY_FILE_NUMBER;
	}
	public Date getDOCUMENT_ISSUE_DATE() {
		return DOCUMENT_ISSUE_DATE;
	}
	public void setDOCUMENT_ISSUE_DATE(Date dOCUMENT_ISSUE_DATE) {
		DOCUMENT_ISSUE_DATE = dOCUMENT_ISSUE_DATE;
	}
	public Date getDOCUMENT_EXPIRY_DATE() {
		return DOCUMENT_EXPIRY_DATE;
	}
	public void setDOCUMENT_EXPIRY_DATE(Date dOCUMENT_EXPIRY_DATE) {
		DOCUMENT_EXPIRY_DATE = dOCUMENT_EXPIRY_DATE;
	}
	public String getCONTACT_FAX() {
		return CONTACT_FAX;
	}
	public void setCONTACT_FAX(String cONTACT_FAX) {
		CONTACT_FAX = cONTACT_FAX;
	}
	public long getKR_STAFF_CODE() {
		return KR_STAFF_CODE;
	}
	public void setKR_STAFF_CODE(long kR_STAFF_CODE) {
		KR_STAFF_CODE = kR_STAFF_CODE;
	}
	public String getCBCM_USERNAME() {
		return CBCM_USERNAME;
	}
	public void setCBCM_USERNAME(String cBCM_USERNAME) {
		CBCM_USERNAME = cBCM_USERNAME;
	}
	public String getADDLN_MAPPING_ID() {
		return ADDLN_MAPPING_ID;
	}
	public void setADDLN_MAPPING_ID(String aDDLN_MAPPING_ID) {
		ADDLN_MAPPING_ID = aDDLN_MAPPING_ID;
	}
	public String getEIDA_FINGERPRINT_FLAG() {
		return EIDA_FINGERPRINT_FLAG;
	}
	public void setEIDA_FINGERPRINT_FLAG(String eIDA_FINGERPRINT_FLAG) {
		EIDA_FINGERPRINT_FLAG = eIDA_FINGERPRINT_FLAG;
	}
	public String getOCR_FLAG() {
		return OCR_FLAG;
	}
	public void setOCR_FLAG(String oCR_FLAG) {
		OCR_FLAG = oCR_FLAG;
	}
	public String getCREATED_OP_USER_ID() {
		return CREATED_OP_USER_ID;
	}
	public void setCREATED_OP_USER_ID(String cREATED_OP_USER_ID) {
		CREATED_OP_USER_ID = cREATED_OP_USER_ID;
	}
	public Date getCREATED_OP_DATE_STAMP() {
		return CREATED_OP_DATE_STAMP;
	}
	public void setCREATED_OP_DATE_STAMP(Date cREATED_OP_DATE_STAMP) {
		CREATED_OP_DATE_STAMP = cREATED_OP_DATE_STAMP;
	}
	public String getUSP_ROLE_ID() {
		return USP_ROLE_ID;
	}
	public void setUSP_ROLE_ID(String uSP_ROLE_ID) {
		USP_ROLE_ID = uSP_ROLE_ID;
	}
	public String getBO_VERIFY_FLAG() {
		return BO_VERIFY_FLAG;
	}
	public void setBO_VERIFY_FLAG(String bO_VERIFY_FLAG) {
		BO_VERIFY_FLAG = bO_VERIFY_FLAG;
	}
	public String getBYPASS_EID_FLAG() {
		return BYPASS_EID_FLAG;
	}
	public void setBYPASS_EID_FLAG(String bYPASS_EID_FLAG) {
		BYPASS_EID_FLAG = bYPASS_EID_FLAG;
	}
	public String getBYPASS_PASSPORT_FLAG() {
		return BYPASS_PASSPORT_FLAG;
	}
	public void setBYPASS_PASSPORT_FLAG(String bYPASS_PASSPORT_FLAG) {
		BYPASS_PASSPORT_FLAG = bYPASS_PASSPORT_FLAG;
	}
	public String getBYPASS_GCCID_FLAG() {
		return BYPASS_GCCID_FLAG;
	}
	public void setBYPASS_GCCID_FLAG(String bYPASS_GCCID_FLAG) {
		BYPASS_GCCID_FLAG = bYPASS_GCCID_FLAG;
	}
	public String getGEO_FENCING_GPS_FLAG() {
		return GEO_FENCING_GPS_FLAG;
	}
	public void setGEO_FENCING_GPS_FLAG(String gEO_FENCING_GPS_FLAG) {
		GEO_FENCING_GPS_FLAG = gEO_FENCING_GPS_FLAG;
	}
}
