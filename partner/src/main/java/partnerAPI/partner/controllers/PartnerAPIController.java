package partnerAPI.partner.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import partnerAPI.partner.model.User;

@RestController
@RequestMapping("/user")
public class PartnerAPIController {

	@RequestMapping(value="/findUsers")
	public ResponseEntity<User> findByUsername(){
		User usr = new User();
		return new ResponseEntity<User>(usr, HttpStatus.OK);
	}
	
	@RequestMapping(value="/testMsg")
	public ResponseEntity<String> testAPI(){
		return new ResponseEntity<String>("Hello BalaJi", HttpStatus.OK);
	}
}
